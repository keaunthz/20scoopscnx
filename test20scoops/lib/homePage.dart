import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test20scoops/model/todo.dart';
import 'package:test20scoops/provider/todoProvider.dart';
import 'package:test20scoops/widget.dart';
import 'package:test20scoops/taskListScreen.dart';

import 'helpers/screen_navigator.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var now = Jiffy(DateTime.now()).yMMMMd;

  late SharedPreferences prefs;

  setupTodo() async {
    final toDoProvider = Provider.of<TodoProvider>(context, listen: false);
    final todos = toDoProvider.todos;
    prefs = await SharedPreferences.getInstance();

    String? stringTodo = prefs.getString('dataTodo');
    if (stringTodo == null) {
      stringTodo = "";
    } else {
      List todoList = jsonDecode(stringTodo);
      for (var todo in todoList) {
        setState(() {
          todos.add(Todo.fromJson(todo));
        });
      }
    }
  }

  @override
  void initState() {
    setupTodo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData = MediaQuery.of(context);
    double screenWidth = queryData.size.width;
    double screenHeight = queryData.size.height;
    final provider = Provider.of<TodoProvider>(context);
    final todos = provider.todos;
    return Scaffold(
      backgroundColor: const Color(0XFF5b89e7),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: const Color(0XFF5b89e7),
        elevation: 0,
        title: Text(widget.title),
      ),
      body: Container(
        padding: const EdgeInsets.all(15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              decoration: const BoxDecoration(
                  color: Color(0xFF0776CF), shape: BoxShape.circle),
              padding: const EdgeInsets.all(6),
              child: const Icon(
                Icons.person,
                color: Colors.white,
              ),
            ),
            welcomeText(todos.length.toString()),
            const SizedBox(
              height: 30,
            ),
            Text(
              'TODAY : ${now.toString()}',
              style: TextStyle(color: Colors.grey[350]),
            ),
            GestureDetector(
              onTap: () {
                changeScreenMaterial(context, const TaskListScreen());
              },
              child: Container(
                width: screenWidth,
                height: screenHeight * 0.52,
                margin: const EdgeInsets.only(top: 10, bottom: 10),
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Hero(
                      tag: 'work icon',
                      child: Container(
                        decoration: const BoxDecoration(
                            color: Color(0xFF0776CF), shape: BoxShape.circle),
                        padding: const EdgeInsets.all(8),
                        child: const Icon(
                          Icons.work,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    WorkTask(width: screenWidth * 0.85)
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Container welcomeText(String todoTask) {
    return Container(
      margin: const EdgeInsets.only(top: 20, bottom: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'Hello John',
            style: TextStyle(
              color: Colors.white,
              fontSize: 30,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Text(
            'Look like feel good.',
            style: TextStyle(color: Colors.grey[350]),
          ),
          Text(
            'You have $todoTask to do today.',
            style: TextStyle(color: Colors.grey[350]),
          ),
        ],
      ),
    );
  }
}
