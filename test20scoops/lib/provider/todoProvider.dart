import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test20scoops/model/todo.dart';

class TodoProvider extends ChangeNotifier {
  late SharedPreferences pref;
  final List<Todo> todos = [
    // Todo(
    //   title: 'Watch movie at cinama',
    //   isDone: false,
    // ),
  ];

  int get isDoneCount => todos.where((todo) => todo.isDone == true).length;

  void addTodo(Todo todo) async {
    todos.add(todo);
    saveData(todos);
    notifyListeners();
  }

  void removeTodo(Todo todo) {
    todos.remove(todo);
    saveData(todos);
    notifyListeners();
  }

  bool checkedTodo(Todo todo) {
    todo.isDone = !todo.isDone;
    saveData(todos);
    notifyListeners();

    return todo.isDone;
  }

  saveData(List<Todo> todos) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String json = jsonEncode(todos);
    prefs.setString('dataTodo', json);
    print('data is ${json}');
  }
}
