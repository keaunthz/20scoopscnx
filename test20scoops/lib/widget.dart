import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:test20scoops/provider/todoProvider.dart';

import 'model/todo.dart';

class TodoWidget extends StatelessWidget {
  final Todo todo;
  const TodoWidget({Key? key, required this.todo}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // ignore: avoid_unnecessary_containers
    return Container(
      margin: const EdgeInsets.only(top: 5),
      child: Column(
        children: [
          Row(
            children: [
              // ignore: avoid_unnecessary_containers
              Checkbox(
                activeColor: Theme.of(context).primaryColor,
                checkColor: Colors.white,
                value: todo.isDone,
                onChanged: (_) {
                  final todoProvider =
                      Provider.of<TodoProvider>(context, listen: false);
                  todoProvider.checkedTodo(todo);
                },
              ),
              TextApp(
                text: todo.title,
                color: todo.isDone ? Colors.black : Colors.grey.shade600,
              )
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          const Divider(
            color: Colors.grey,
          )
        ],
      ),
    );
  }
}

class WorkTask extends StatelessWidget {
  final double width;
  const WorkTask({Key? key, required this.width}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<TodoProvider>(context);
    final todos = provider.todos;
    final textPercent = (provider.isDoneCount.toDouble() / todos.length) * 100;

    return Container(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "${todos.length} Task",
          style: const TextStyle(
            color: Colors.grey,
          ),
        ),
        const Text(
          'Works',
          style: TextStyle(
            color: Color(0xff615F61),
            fontSize: 30,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 10, bottom: 10),
          child: LinearPercentIndicator(
            width: width,
            padding: const EdgeInsets.symmetric(horizontal: 0),
            barRadius: const Radius.circular(10),
            lineHeight: 3.5,
            percent: provider.isDoneCount.toDouble() / todos.length,
            backgroundColor: Colors.grey[300],
            progressColor: const Color(0XFF5b89e7),
          ),
        ),
        Text(
          todos.isEmpty
              ? "0% to completed"
              : "${textPercent.toStringAsFixed(0)}% to completed",
          style: const TextStyle(
            color: Colors.grey,
          ),
        ),
      ],
    ));
  }
}

class TextApp extends StatelessWidget {
  double size;
  bool isFontweight;
  final String text;
  final Color color;

  TextApp(
      {Key? key,
      this.size = 14,
      required this.text,
      this.color = Colors.black,
      this.isFontweight = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
          color: color,
          fontSize: size,
          fontWeight:
              isFontweight == true ? FontWeight.bold : FontWeight.normal),
    );
  }
}
