import 'package:test20scoops/addTaskScreen.dart';

class Todo {
  String? id;
  String title = '';
  bool isDone = false;

  Todo({
    this.id,
    required this.title,
    this.isDone = false,
  });

  Map<String, dynamic> toJson() {
    return {'id': id, 'title': title, 'isDone': isDone};
  }

  Todo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    isDone = json['isDone'];
  }
}
