import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:provider/provider.dart';
import 'package:test20scoops/provider/todoProvider.dart';
import 'package:test20scoops/widget.dart';

import 'model/todo.dart';

class AddTaskScreen extends StatefulWidget {
  const AddTaskScreen({Key? key}) : super(key: key);

  @override
  State<AddTaskScreen> createState() => _AddTaskScreenState();
}

class _AddTaskScreenState extends State<AddTaskScreen> {
  String textForm = '';
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final toDoProvider = Provider.of<TodoProvider>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: const Icon(Icons.close, color: Colors.grey),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            TextApp(text: "Whats tasks are you planning to perform?"),
            Form(
              key: _formKey,
              child: TextFormField(
                maxLines: 1,
                initialValue: '',
                onChanged: (title) => setState(() => textForm = title),
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                ),
                validator: (title) {
                  if (title!.isEmpty) {
                    return 'The title cannot be empty';
                  }
                  return null;
                },
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: GestureDetector(
        onTap: () {
          final isValid = _formKey.currentState!.validate();
          if (!isValid) {
            return;
          } else {
            toDoProvider.addTodo(
              Todo(
                id: DateTime.now().toString(),
                title: textForm,
                isDone: false,
              ),
            );
            Navigator.pop(context);
          }
        },
        child: Container(
          width: double.infinity,
          height: 50,
          decoration: const BoxDecoration(color: Colors.blue),
          child: const Center(
            child: Icon(
              Icons.add,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
