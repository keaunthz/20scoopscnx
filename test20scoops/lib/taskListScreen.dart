import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:provider/provider.dart';
import 'package:test20scoops/helpers/screen_navigator.dart';
import 'package:test20scoops/provider/todoProvider.dart';
import 'package:test20scoops/addTaskScreen.dart';
import 'package:test20scoops/widget.dart';
import 'package:percent_indicator/percent_indicator.dart';

class TaskListScreen extends StatefulWidget {
  const TaskListScreen({Key? key}) : super(key: key);

  @override
  State<TaskListScreen> createState() => _TaskListScreenState();
}

class _TaskListScreenState extends State<TaskListScreen> {
  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData = MediaQuery.of(context);
    double screenWidth = queryData.size.width;
    double screenHeight = queryData.size.height;
    final provider = Provider.of<TodoProvider>(context);
    final todos = provider.todos;
    return Scaffold(
      // ignore: avoid_unnecessary_containers
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.grey),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(22),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Hero(
              tag: 'work icon',
              child: Container(
                decoration: const BoxDecoration(
                    color: Color(0xFF0776CF), shape: BoxShape.circle),
                padding: const EdgeInsets.all(8),
                margin: const EdgeInsets.only(bottom: 20),
                child: const Icon(
                  Icons.work,
                  color: Colors.white,
                ),
              ),
            ),
            WorkTask(
              width: screenWidth * 0.6,
            ),
            const SizedBox(
              height: 30,
            ),
            Expanded(
              child: todos.isEmpty
                  ? Center(
                      child: TextApp(
                        text: "Add your first task!",
                        color: const Color(0xff615F61),
                      ),
                    )
                  : ListView.builder(
                      itemCount: todos.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Dismissible(
                            direction: DismissDirection.endToStart,
                            background: Container(
                              padding: const EdgeInsets.only(right: 20),
                              color: Colors.redAccent,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  TextApp(
                                    text: "Swipe to remove task",
                                    color: Colors.white,
                                  ),
                                ],
                              ),
                            ),
                            onDismissed: (direction) {
                              if (direction == DismissDirection.endToStart) {
                                provider.removeTodo(todos[index]);

                                Scaffold.of(context).showSnackBar(
                                  const SnackBar(
                                    content: Text("Remove some task!"),
                                    duration: Duration(seconds: 1),
                                  ),
                                );
                              }
                            },
                            key: Key(todos[index].id.toString()),
                            child: TodoWidget(todo: todos[index]));
                      }),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(
          Icons.add,
          size: 35,
        ),
        onPressed: () => changeScreenMaterial(context, const AddTaskScreen()),
      ),
    );
  }
}
