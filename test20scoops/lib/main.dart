import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jiffy/jiffy.dart';
import 'package:provider/provider.dart';
import 'package:test20scoops/helpers/screen_navigator.dart';
import 'package:test20scoops/homePage.dart';
import 'package:test20scoops/provider/todoProvider.dart';
import 'package:test20scoops/taskListScreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => ListenableProvider(
        create: (context) => TodoProvider(),
        child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            textTheme: GoogleFonts.nunitoSansTextTheme(
              Theme.of(context).textTheme,
            ),
            primarySwatch: Colors.blue,
            unselectedWidgetColor: const Color(0XFF5b89e7),
          ),
          debugShowCheckedModeBanner: false,
          home: const HomePage(title: "Todo"),
        ),
      );
}
